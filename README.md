# Ultralite

Resources for a blog at https://ultralite.dev

## Terraform

Terraform code uses two modules, both are published in the registry.

## CloudFormation

CloudFormation code assumes one hosted zone in the parameters file. Right now I
am testign this in a lab, so that will be interpolated once I move it to CI/CD.

