provider "aws" {}

variable "site_name" {
  default = "ultralite.dev"
}

resource "aws_route53_zone" "site_zone" {
  name = var.site_name
}

resource "aws_route53_record" "mx_records" {
  zone_id = aws_route53_zone.site_zone.zone_id
  name    = var.site_name
  type    = "MX"
  ttl     = "5"
  records = [
    "1 aspmx.l.google.com",
    "5 alt1.aspmx.l.google.com",
    "5 alt2.aspmx.l.google.com",
    "10 alt3.aspmx.l.google.com",
    "10 alt4.aspmx.l.google.com"
  ]
}

resource "aws_route53_record" "google_verify" {
  zone_id = aws_route53_zone.site_zone.zone_id
  name    = var.site_name
  type    = "TXT"
  ttl     = 30
  records = ["google-site-verification=XvO0bZSJ-ocSH_u2lktpz5QQV8ZLfbGzRRazysHyk7o"]
}

module "acm" {
  source  = "terraform-aws-modules/acm/aws"
  version = "~> v2.0"
  domain_name  = var.site_name
  zone_id      = aws_route53_zone.site_zone.zone_id
  subject_alternative_names = [
    "*.${var.site_name}",
  ]
  tags = {
    Name = var.site_name
  }
}

module "cloudfront_s3_website_with_domain" {
  source                 = "chgangaraju/cloudfront-s3-website/aws"
  version                = "1.2.2"
  hosted_zone            = var.site_name
  domain_name            = var.site_name
  acm_certificate_domain = var.site_name
  upload_sample_file     = true
}
