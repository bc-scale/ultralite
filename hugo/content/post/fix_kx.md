---
title: Fix for bad update that affects KX drivers
author: Jesse Perry
date: "2021-03-19"
categories:
- HowTos
tags:
- CMD
- WINDOWS
draft: no
---
# Fix for bad update that affects KX drivers

![Windows Updates are a dumpster fire](https://emojis.slackmojis.com/emojis/images/1566825215/6248/dumpster-fire.gif)

Microsoft has released a patch that causes BSOD when printing to Kyocera
printers, among others. This update has been addressed in a variety of ways,
from tweaks to the kx driver to installation of a hotfix. These attempts to
address the issue have met with limited success. You are KILLING ME Microsoft!

Here are some ways to find and remove the offending update, as well as a more
nuclear route to disable Windows Update entirely to prevent it from coming back
on again despite your best efforts to postpone or disable updates in the UI.

## Find the affected update

The offending updates are `kb5000802` and `kb5000808`, depending on which
version of Windows 10 you have. First you can find the presence of these updates
with the WMIC command below and piped through find.

```batch
wmic qfe | find "500080"
```

## Uninstall the patch from the command line using WUSA

The `wusa` app is the offline installer and can be used to surgically remove
patches. This is a very good tool to be familiar with. Is it too much to ask
that this app work as advertised? Is it too much to ask that `/quiet` and
`/warnrestart:600` would actually do what the help docs at `wusa /?` say?
Evidently, thanks again Microsoft. :dumpster-fire: For this reason you will have
to let it interact with the user session.


```batch
wusa /uninstall /kb:5000802
```

## Disable Windows Update Service 💣

Disabling Windows Updates from the UI doesn't always meet with the expected
results, I can't count how many times I have told Windows NOT to update only to
find it plowing on ahead paying no heed to the setting. So, when this happens I
have to go the nuclear route, disabling the Windows Update Service directly.
This is a bad practice, it is very easy to forget and find a system unpatched
long after this setting was made. One of these days Microsoft's patching system
will go from a full-on dumpster fire to something more like the rest of
civilized patching, like Linux perhaps. Until then, we have to do what we have
to do. So, let's nuke Windows Updates.

```batch
net stop wuauserv
sc config wuauserv start= disabled
```

## When disabling windows update doesn't work (which is a lot)

Windows has a nasty habit of disregarding your wishes and installing updates
despite your best efforts. ARGH! So we will need to hide the update to force it
not be be installed.

First you need to install a powershell module, but that can be a problem when using a
remote shell that doesn't give you the input prompts to accept a license or
approve an install from a particular repo. So let's first install the
prerequisites to install a module from the repository.

### Install the NuGet Package Provider

The NuGet Package Provider will allow you to install powershell modules from the
terminal. First you install this provider.

```powershell
Install-PackageProvider -Name NuGet -Force
```

And let's make the default microsoft PSGallery repo Trusted.

```powershell
Set-PSRepository PSGallery -InstallationPolicy Trusted
```

### Install the PSWindowsUpdate module

Now let's install the windows update powershell module.

```powershell
Install-Module PSWindowsUpdate -force
```

Let's see if it worked, shall we? Now we query the windows update system to see
what updates are available, if we get results then we know it worked.

```powershell
Get-WUList
...
ComputerName Status     KB          Size Title
------------ ------     --          ---- -----
Win10TestPC    -------    KB4601556   77MB 2021-02 Cumulative Update Preview for .NET Framework 3.5 and 4.8 for Windows...
Win10TestPC    -D-----    KB4577586   20KB Update for Removal of Adobe Flash Player for Windows 10 Version 1909 for x64...
Win10TestPC    -------    KB4589211    3MB 2021-01 Update for Windows 10 Version 1909 for x64-based Systems (KB4589211)
Win10TestPC    -D-----                 4MB Brother - Printer - 4/22/2009 12:00:00
AM - 10.0.17119.1
```

### Hide offending windows updates

You can now hide the offending update by the KB number as shown below. It would
be a good idea to check afterwards if that update has indeed been hidden, run
the `Get-WUList` again and look for your update.

```powershell
Hide-WindowsUpdate -KBArticleId KB5000802 -AcceptAll
```


### Let's see that all put together

```powershell
# Install the NuGet package provider
  C:\> Install-PackageProvider -Name NuGet -Force
  Name                           Version          Source           Summary
  ----                           -------          ------           -------
  nuget                          2.8.5.208        https://onege... NuGet provider for the OneGet meta-package manager

# Trust the PSGallery repo
  C:\> Set-PSRepository PSGallery -InstallationPolicy Trusted
  Microsoft.PackageManagement.Packaging.SoftwareIdentity

# Install the PSWindowsUpdate module
  C:\> Install-Module PSWindowsUpdate -force

# Test the module and find the updates
  C:\> Get-WUList
  ComputerName Status     KB          Size Title
  ------------ ------     --          ---- -----
  Win10TestPC -------    KB4589212    3MB 2021-01 Update for Windows 10 Version 2004 for x64-based Systems (KB4589212)
  Win10TestPC -D-----    KB5000802   85GB 2021-03 Cumulative Update for Windows 10 Version 2004 for x64-based Systems ...

# Hide the updates. Note the 'D--H--' indicates the update is now 'H'idden
  C:\> Hide-WindowsUpdate -KBArticleId KB5000802 -AcceptAll
  ComputerName Status     KB          Size Title
  ------------ ------     --          ---- -----
  Win10TestPC D--H--     KB5000802   85GB 2021-03 Cumulative Update for Windows 10 Version 2004 for x64-based Systems ...

# Verify the update is gone
  C:\> Get-WUList
  ComputerName Status     KB          Size Title
  ------------ ------     --          ---- -----
  Win10TestPC -------    KB4589212    3MB 2021-01 Update for Windows 10 Version 2004 for x64-based Systems (KB4589212)
```
