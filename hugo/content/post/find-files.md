---
title: Find files modified after a specific date
author: Jesse Perry
date: "2021-01-27"
categories:
- HowTos
tags:
- CMD
- BASH
draft: no
---
# Finding files on your system doesn't have to be about clicking...

Since the wayback days I have found more success in quickly finding files in
Windows from the cmd prompt rather than Explorer. My typical recipe for success
is to open a command prompt as administrator, then `cd \` to the root, and then
search for what I am looking for with `dir /s *.qbw`. This nets me a nice easy
to read list of files. That is usually enough. But sometimes I need to find
something a little more specific. Let's look at a few examples.


## Refining a search to give me less results

Let's say I want to do that same search in either Linux (including MacOS) or in
Windows, but only look for files that include `JPTechnical` in the name. In
Windows I would then pipe that search to the `find` command, ala `dir /s *.qbw |
find "JPTechnical"`. In Linux, it would look a little different, `find . -iname
'*.md'` would find all the `.md` files in the current directory, I would pipe
this into `grep` to filter that with `find . -iname '*.md' | JPTechnical` for
find any Markdown format files with `JPTechnical` in the name.

This is also fairly simple, it is just a little magic with pipes. Let's dig
deeper with a more complex search.

## Finding files modified AFTER a specific date

Let's imagine you are backing up your computer, and you have a BUNCH of extra
files that you know you haven't touched in years. As a matter of fact, I am
doing just that for a client right now, I am looking for recently modified files
so that their new computer will not have so much cruft left over from so many
years ago. So let's look at how to find files modified after a specific date on both Linux
(and MacOS) systems as well as Windows systems. 

### In Linux using the `find` command...

To search for any files in my `git` folder in my `home` folder modified since
yesterday I would use the following search:

```bash
find ~/git -newermt '2021-01-26T00:00:00'
```

What does this do?

- `-newermt` is looking for anything *newer* than the *modified time* that
  follows. 

Here is a great [cheat sheet](http://cheat.sh/find) for how to use the find
command.

### In Windows Command Prompt...

To search for any files in a specific user's Desktop that has been modified
AFTER 1-1-2019, use the following search: 
```cmd
c:\>forfiles /P c:\users\jp\Desktop /S /D +01/01/2019
```

What does this do? 

- `/P` is where you specifiy the `<pathname>`, in our case it is `c:\users\jp\Desktop`. 
- `/S` is recursive
- `/D +01/01/2019` is the date later than, as specified by the `+` the date.

For the full docs, [see here](https://ss64.com/nt/forfiles.html), and here is a
great [cheat sheet](http://cheat.sh/forfiles).


## Embrace the console!

Searching can be quite a bit more enjoyable than double-clicking your way
through endless folders in Windows Explorer. Any of these commands can be
redirected to a file that can be further searched, or retained for other use.
Embrace the console!
