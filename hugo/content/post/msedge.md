---
title: Silent uninstaller for Edge Chromium
author: Jesse Perry
date: "2021-04-13"
categories:
- HowTos
tags:
- CMD
- WINDOWS
draft: no
---
# Finally, a *truly* silent uninstaller for Microsoft Edge Chromium

I applaud Microsoft for taking Google's Chromium project and building a decent
web browser around it, finally the death of Internet Explorer! Unfortunately, I
have found that many people I work with unknowingly start using the new Edge
browser because Microsoft forced it on them. This is bad form Microsoft, a dirty
trick. The reason this is a problem is that *most* of the people I work with
use Google Workplace products instead of Microsoft 365, and they don't realize
that by switching away from Chrome, even though it is Chrome(ish) based, the
synchronization with Google Workplace and your Chrome profile does not work in
other browsers. Fast forward a year or so and something happens to their Windows
install and they *assume* everything is backed up like it always has been, only
they aren't syncing anymore. This is sad, and totally preventable if Microsoft
would stop forcing their browser on people. Let us choose!

Anyway, that leads up to the search for an uninstaller for Edge, that can be
done in an orchestrated fashion and easily applied regardless of what
orchestrator or management system you use, so a script seems like a natural
conclusion. But, while it makes perfect sense to me (and I image you since you
are reading this), it is lost on Microsoft. So, their so-called silent
uninstaller requires interaction with the desktop, by way of popping up a
browser to tell you what a terrible decision it is to remove Edge, and how you
are going to break the internet, burn down your computer and possibly kill a
puppy by proceeding. So, you have to close the window to finish the uninstaller. 

I am super excited that I finally figured out how to automate it without having
to interact with the desktop. So here we go, buckle up, this script is ugly but
it works!

## Find the uninstaller

You first of all have to find the uninstaller, because the path to the `setup.exe`
is in a path that changes because the version number is in the path. You can
find the version number with the `dir` command, then pipe it to `find "8"` to
give you only lines that include the number 8 (currently the version is
8.9x.xx), then add it to a full installer path with a for loop. Like I said, ugly.
Here is the code below. 

```batch
set MSEDGEPATH=c:\Program Files (x86)\Microsoft\Edge\Application
for /f "tokens=*" %%i in ('dir /b "%MSEDGEPATH%" ^|find "8"') do set MSEDGEVER=%%i
set MSEDGESETUP="%MSEDGEPATH%\%MSEDGEVER%\Installer\setup.exe"
```

## Deal with Edge running in the background

You would think you could just run the uninstall now, and it would work. NOPE!
If Edge is running in the background, it's *always* running in the background
🙄, you will need to kill it or acknowledge a message. So let's kill any version
of Edge with `taskkill /im:MsEdge.exe /f` and `taskkill /im:MicrosoftEdge.exe
/f`. Then you can run the uninstaller, so we let both of those commands first
and ignore any errors if the program isn't running, then we run it after, for
good measure. Here is the code for the uninstaller with the task killing.

```batch
:uninstall
  echo Uninstalling MSEdge Chromium found at %MSEDGESETUP%
  taskkill /im:MsEdge.exe /f
  taskkill /im:MicrosoftEdge.exe /f
  %MSEDGESETUP% --uninstall --system-level --verbose-logging --force-unistall
  taskkill /im:MsEdge.exe /f
  taskkill /im:MicrosoftEdge.exe /f
```

## I will not take no for an answer

I have read post after post on the interwebs of people frustrated that the
installer *still* prompts you despite the claim to be a silent uninstaller, or
that the command just doesn't work. This is because, in my experience, when the uninstaller runs
you will get another browser window begging you not to uninstall Edge.
Microsoft, you are so needy, GOSH!

So here is the rub, if you try this from a remote shell the command
hangs waiting for someone to close the browser window. So even still it won't run truly silently. A workaround is to open
*another* shell and then do the `taskkill` commands, then the command
completes. That's LAME. So how do we do that in a script? We can start a new
`cmd` process that sleeps for 30 seconds and then runs the task kill command.

When I promised ugly, I meant it... this is UGLY! But, you know what, it works.
So here it is, the full silent uninstaller for Microsoft Edge Chromium. I hope
this saves someone some time, if so please share it with someone, or give my
business a [Google review](https://g.page/r/CcMcHymyhgMIEAg/review). 

```batch
@ECHO OFF
  set MSEDGEPATH=c:\Program Files (x86)\Microsoft\Edge\Application
  for /f "tokens=*" %%i in ('dir /b "%MSEDGEPATH%" ^|find "8"') do set MSEDGEVER=%%i
  set MSEDGESETUP="%MSEDGEPATH%\%MSEDGEVER%\Installer\setup.exe"

:block
  echo Blocking MSEdge Chromium from installing blocker toolkit...
  start \jptechnical\disable_edgechromium.cmd /B
  goto :uninstall

:uninstall
  echo Uninstalling MSEdge Chromium found at %MSEDGESETUP%
  taskkill /im:MsEdge.exe /f
  taskkill /im:MicrosoftEdge.exe /f
  start cmd /c "timeout 30 && taskkill /im:MsEdge.exe /f"
  start cmd /c "timeout 30 && taskkill /im:MicrosoftEdge.exe /f"
  %MSEDGESETUP% --uninstall --system-level --verbose-logging --force-unistall
  start cmd /c "timeout 30 && taskkill /im:MicrosoftEdge.exe /f"
  start cmd /c "timeout 30 && taskkill /im:MsEdge.exe /f"
  taskkill /im:MsEdge.exe /f
  taskkill /im:MicrosoftEdge.exe /f
  goto :end

:end
```
## Bonus: Blocking Microsoft Edge Chromium from installing

After all this hard work, wouldn't it be a bummer if Edge came back again on the
next update? Yeah, it is, a major bummer. This
[script](https://www.bleepingcomputer.com/news/microsoft/how-to-block-windows-10-update-force-installing-the-new-edge-browser/)
is referenced above, and it blocks the future install. So far it *seems* to be
working. 
