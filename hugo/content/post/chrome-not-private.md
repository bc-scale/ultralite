---
title: Defeat 'Your connection is not private' in Chrome
author: Jesse Perry
date: "2021-03-06"
categories:
- HowTos
tags:
- HTTPS
- Chrome
draft: no
---
# How to get past this warning message in Chrome

{{< caption-lg src="/images/your_connection_is_not_private.png" caption="Chrome's Your connection is not private message" >}}

Chrome has done a lot to save me from myself when it comes to safety online. One
example of this is the warning when a website has an invalid certificate. But,
there are some legitimate circumstances where you want to bypass this message,
and it is not readily apparent. This post will give you a quick way to bypass
this message. 

When stuck on this message you can type `thisisunsafe` when viewing this message
and it will add an exclusion for you. BOOM! 

## Allow all localhost links

For testing I will often need to make this exclusion for local stuff running in
containers. You can make a manual exclusion for localhost by going to
"chrome://flags/#allow-insecure-localhost" and clicking the "Enable" link.

